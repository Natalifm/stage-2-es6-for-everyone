import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  firstFighter.currentHealth = firstFighter.health;
  secondFighter.currentHealth = secondFighter.health;
  return new Promise((resolve) => {
    const winnerFighter = function() {
      if (firstFighter.health <= 0 || secondFighter.health <= 0) {
        let winner = firstFighter.health <= 0 ? secondFighter : firstFighter;

        for (let someFunction of pressedKeys) {
          someFunction();
        }
        resolve(winner);
      }
    };

    let pressedKeys = [];
    let firstFighterBlock = false;
    const firstFighterBlockStart = function() {
      firstFighterBlock = true;
    };
    const firstFighterBlockEnd = function() {
      firstFighterBlock = false;
    };
    pressedKeys.push(registerKeyHandler(controls.PlayerOneBlock, firstFighterBlockStart, firstFighterBlockEnd));

    let secondFighterBlock = false;
    const secondfighterBlockStart = function() {
      secondFighterBlock = true;
    };
    const secondFighterBlockEnd = function() {
      secondFighterBlock = false;
    };
    pressedKeys.push(registerKeyHandler(controls.PlayerTwoBlock, secondfighterBlockStart, secondFighterBlockEnd));

    const firstFighterAttack = function() {
      if (!secondFighterBlock && !firstFighterBlock) {
        secondFighter.health -= getDamage(firstFighter, secondFighter);
        updateFightersHealth(firstFighter, secondFighter);
        winnerFighter();
      }
    };
    pressedKeys.push(registerKeyHandler(controls.PlayerOneAttack, firstFighterAttack));

    const secondFighterAttack = function() {
      if (!firstFighterBlock && !secondFighterBlock) {
        firstFighter.health -= getDamage(secondFighter, firstFighter);
        updateFightersHealth(firstFighter, secondFighter);
        winnerFighter();
      }
    };
    pressedKeys.push(registerKeyHandler(controls.PlayerTwoAttack, secondFighterAttack));

    const firstFighterCriticalHit = function() {
      secondFighter.health -= getCriticalHit(firstFighter);
      updateFightersHealth(firstFighter, secondFighter);
      winnerFighter();
    };
    pressedKeys.push(registerMultipleKeyHandler(...controls.PlayerOneCriticalHitCombination, firstFighterCriticalHit));

    const secondFighterCriticalHit = function() {
      firstFighter.health -= getCriticalHit(secondFighter);
      updateFightersHealth(firstFighter, secondFighter);
      winnerFighter();

    };
    pressedKeys.push(registerMultipleKeyHandler(...controls.PlayerTwoCriticalHitCombination, secondFighterCriticalHit));
  });
}

function updateFightersHealth(firstFighter, secondFighter) {
  let firstFighterIndicator = document.getElementById('right-fighter-indicator');
  let secondFighterIndicator = document.getElementById('left-fighter-indicator');

  let firstFighterHealth = secondFighter.health * 100 / secondFighter.currentHealth;
  firstFighterIndicator.style.background = `linear-gradient(90deg, #ebd759 ${firstFighterHealth}%, blue 0)`;

  let secondFighterHealth = firstFighter.health * 100 / firstFighter.currentHealth;
  secondFighterIndicator.style.background = `linear-gradient(90deg, #ebd759 ${secondFighterHealth}%, blue 0)`;
}

export function getDamage(attacker, defender) {
  // return damage
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  return blockPower > hitPower ? 0 : hitPower - blockPower;

}

export function getHitPower(fighter) {
  // return hit power
  let criticalHitChance = Math.random() + 1;
  return fighter.attack * criticalHitChance
}

export function getBlockPower(fighter) {
    // return block power
  let dodgeChance = Math.random() + 1;
  return dodgeChance * fighter.defense;

}

function getCriticalHit(fighter) {
  return 2 * fighter.attack;
}

function registerMultipleKeyHandler(key1, key2, key3, downCallback) {
  let criticalHitKey1 = false;
  let criticalHitKey2 = false;
  let criticalHitKey3 = false;
  let lastCallbackDate = null;

  function check() {
    if (criticalHitKey1 && criticalHitKey2 && criticalHitKey3) {
      const dateNow = Date.now();
      if (!lastCallbackDate || dateNow - lastCallbackDate > 10000) {
        lastCallbackDate = dateNow;
        downCallback();
      }
    }
  }

  const down1 = function() {
    criticalHitKey1 = true;
    check();
  };
  const down2 = function() {
    criticalHitKey2 = true;
    check();
  };
  const down3 = function() {
    criticalHitKey3 = true;
    check();
  };
  const up1 = function() {
    criticalHitKey1 = false;
  };
  const up2 = function() {
    criticalHitKey2 = false;
  };
  const up3 = function() {
    criticalHitKey3 = false;
  };

  const removePressedKey1 = registerKeyHandler(key1, down1, up1);
  const removePressedKey2 = registerKeyHandler(key2, down2, up2);
  const removePressedKey3 = registerKeyHandler(key3, down3, up3);

  return () => {
    removePressedKey1();
    removePressedKey2();
    removePressedKey3();

  };
};

function registerKeyHandler(key, downCallback, upCallback) {

  const keyDown = function(event) {
    if (downCallback && event.code == key && event.repeat === false) {
      downCallback();
    }
  };

  const keyUp = function(event) {
    if (upCallback && event.code == key) {
      upCallback();
    }
  };

  document.addEventListener('keydown', keyDown);
  document.addEventListener('keyup', keyUp);

  const removePressedKey = () => {
    document.removeEventListener('keydown', keyDown);
    document.removeEventListener('keyup', keyUp);
  };
  return removePressedKey;
}

