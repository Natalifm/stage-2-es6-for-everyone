import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
  // call showModal function
  const result = {
    title: `Congratulation, ${fighter.name}, you are the winner`,
    bodyElement: createFighterImage(fighter),
    onClose: () => {
      window.location.reload();
    },
  };
  showModal(result);
}

